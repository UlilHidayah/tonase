<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Errorhandler extends Model
{
    protected $table = 'errorhandler';
    protected $primaryKey = 'id';

    public $timestamps = false;
}
