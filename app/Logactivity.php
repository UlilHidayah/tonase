<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logactivity extends Model
{
    protected $table = 'logactivities';
    protected $primaryKey = 'id';
}
