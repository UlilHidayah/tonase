<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trandetail extends Model
{
    protected $table = 'trandetail';
    protected $primaryKey = 'id';
}
