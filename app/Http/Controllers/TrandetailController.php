<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;

use App\Trandetail;
use App\Saldo;
use App\User;
use App\Errorhandler;
class TrandetailController extends Controller
{
    public function topup(Request $request)
    {
        // Insert logactivity data into table
        $saldo = Saldo::where('id_user', Auth::user()->id)->first();
        $saldo = (float)$saldo->saldo + (float)$request->saldo;

        $result = Saldo::where('id_user', Auth::user()->id)->update([
            'saldo' => $saldo,
        ]);
        $td = new Trandetail();
        $td->id_user = Auth::user()->id;
        $td->jenis_transaksi = 'Top Up';
        $td->id_tujuan = Auth::user()->id;
        $td->nominal = $request->saldo;
        $td->keterangan = 'Top Up Saldo';
        $td->status = 'Success';
        $td->save();

        return back()->with('alert', 'success');
    }

    public function penarikan(Request $request)
    {
        // Insert logactivity data into table
        $saldo = Saldo::where('id_user', Auth::user()->id)->first();
        $saldo = (float)$saldo->saldo - (float)$request->saldo;

        $result = Saldo::where('id_user', Auth::user()->id)->update([
            'saldo' => $saldo,
        ]);
        $td = new Trandetail();
        $td->id_user = Auth::user()->id;
        $td->jenis_transaksi = 'Penarikan';
        $td->id_tujuan = Auth::user()->id;
        $td->nominal = $request->saldo;
        $td->keterangan = 'Penarikan Saldo';
        $td->status = 'Success';
        $td->save();

        return back()->with('alert', 'success');
    }

    public function transfer(Request $request)
    {
        $usertujuan = User::where('id', $request->id_tujuan)->first();
        if(!$usertujuan){
            $err = Errorhandler::where('kode', 1064)->first();
            session(['msg' => $err->message]);
            return back()->with('alert', 'failed');
        }
        $saldo = Saldo::where('id_user', Auth::user()->id)->first();
        $saldotujuan = Saldo::where('id_user', $request->id_tujuan)->first();

        if($saldo->saldo < (float)$request->saldo){
            return back()->with('alert', 'failed');
        }

        $saldo1 = (float)$saldo->saldo - (float)$request->saldo;
        $saldo2 = (float)$saldotujuan->saldo + (float)$request->saldo;

        $result = Saldo::where('id_user', Auth::user()->id)->update([
            'saldo' => $saldo1,
        ]);
        $result = Saldo::where('id_user', $request->id_tujuan)->update([
            'saldo' => $saldo2
        ]);
        $td = new Trandetail();
        $td->id_user = Auth::user()->id;
        $td->jenis_transaksi = 'Debit';
        $td->id_tujuan = $request->id_tujuan;
        $td->nominal = $request->saldo;
        $td->keterangan = 'InhouseTrf CSCS dari ' . strtoupper(Auth::user()->name) . ' ' . $request->keterangan;
        $td->status = 'Success';
        $td->save();

        $td = new Trandetail();
        $td->id_user = Auth::user()->id;
        $td->jenis_transaksi = 'Transfer';
        $td->id_tujuan = Auth::user()->id;
        $td->nominal = $request->saldo;
        $td->keterangan = 'InhouseTrf CSCS ke ' . strtoupper($usertujuan->name) . ' ' . $request->keterangan;
        $td->status = 'Success';
        $td->save();

        return back()->with('alert', 'success');
    }
}