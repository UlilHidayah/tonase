<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'Auth\AuthController@showFormLogin')->name('login');
Route::get('login', 'Auth\AuthController@showFormLogin')->name('login');
Route::post('login', 'Auth\AuthController@login');
Route::get('register', 'Auth\AuthController@showFormRegister')->name('register');
Route::post('register', 'Auth\AuthController@register');

Route::group(['middleware' => 'auth'], function () {
    // Route::get('home', function () {
    //     return view('welcome');
    // })->name('home');
    Route::get('home', 'Auth\AuthController@home')->name('home');
    Route::get('logout', 'Auth\AuthController@logout')->name('logout');
    Route::get('transaksi/topup', function () {
        return view('topup');
    })->name('form.topup');
    Route::post('prosestopup', 'TrandetailController@topup')->name('transaksi.topup');
    Route::get('transaksi/tarik', function () {
        return view('tarik');
    })->name('form.tarik');
    Route::post('prosestarik', 'TrandetailController@penarikan')->name('transaksi.tarik');
    Route::get('transaksi/transfer', function () {
        return view('transfer');
    })->name('form.transfer');
    Route::post('prosestransfer', 'TrandetailController@transfer')->name('transaksi.transfer');
});
