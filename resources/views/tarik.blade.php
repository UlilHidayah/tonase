<html>
<head>
    <title>Penarikan</title></head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<body>
    @if(session()->has('alert'))
    @if(session()->get('alert') == 'success')
    <div class="alert alert-success">
        <strong>Success!</strong> Successfully!
    </div>
    @else
    <div class="alert alert-danger">
        <strong>Error!</strong> Gagal!
    </div>
    @endif
    @endif
    <a href="{{ route('home') }}">Go to dashboard</a>
    <br/><br/>

    <form action="{{ route('transaksi.tarik') }}" method="POST" name="form1">
        {{ csrf_field() }}
        <table width="50%" border="0">
            <tr> 
                <td>Jumlah</td>
                <td><input type="numeric" name="saldo" required onkeypress="return hanyaAngka(event)" autocomplete="off"><br/><br/></td>
            </tr>
            <tr> 
                <td></td>
                <td><input type="submit" name="Submit" value="Tarik"></td>
            </tr>
        </table>
    </form>
</body>
</html>
<script> 
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
</script> 