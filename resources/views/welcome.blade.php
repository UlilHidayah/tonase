<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        table {
            width:100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 15px;
            text-align: left;
            color: black;
        }
        #t01 tr:nth-child(even) {
            background-color: #eee;
        }
        #t01 tr:nth-child(odd) {
            background-color: #fff;
        }
        #t01 th {
            background-color: grey;
            color: white;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('logout') }}" class="btn btn-danger" style="float: right">Logout</a>
                    <h3>Dashboard</h3>
                </div>
                <div class="card-body">
                    <h5>Selamat datang di halaman dashboard, <strong>{{ Auth::user()->name }}</strong></h5>
                    <h5>ID Akun <strong>{{ Auth::user()->id }}</strong></h5>
                    <h5>Saldo anda <strong>IDR {{ $saldo }}</strong></h5>
                    <a href="{{ route('form.topup') }}" class="btn btn-primary">Top Up</a>
                    <a href="{{ route('form.tarik') }}" class="btn btn-warning">Penarikan</a>
                    <a href="{{ route('form.transfer') }}" class="btn btn-success">Transfer</a>
                    <br><br>
                    <h3>History Transaksi</h3>

                    <table style="width:100%;" id="t01">
                        <tr>
                            <th>Tanggal</th>
                            <!-- <th>Transaksi</th>
                            <th>ID Tujuan/Dari</th>  -->
                            <th>Nominal</th>
                            <th>Keterangan</th>
                            <th>Status</th>
                        </tr>
                        
                        <?php foreach ($trandetail as $t): ?>
                            <tr>
                                <td><?php echo date("d F Y h:i:s", strtotime($t->updated_at)); ?></td>
                                <!-- <td><?php echo $t->jenis_transaksi; ?></td>
                                <td><?php echo $t->id_tujuan; ?></td> -->
                                <td><?php echo $t->nominal; ?></td>
                                <td><?php echo $t->keterangan; ?></td>
                                <td><?php echo $t->status; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
