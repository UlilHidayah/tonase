<html>
<head>
    <title>Transfer</title></head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<body>
    @if(session()->has('alert'))
    @if(session()->get('alert') == 'success')
    <div class="alert alert-success">
        <strong>Success!</strong> Successfully!
    </div>
    @else
    <div class="alert alert-danger">
        <strong>400 </strong> <?php echo session('msg') ?>
    </div>
    @endif
    @endif
    <a href="{{ route('home') }}">Go to dashboard</a>
    <br/><br/>

    <form action="{{ route('transaksi.transfer') }}" method="POST" name="form1">
        {{ csrf_field() }}
        <table width="50%" border="0">
            <tr> 
                <td>ID Tujuan</td>
                <td><input type="text" name="id_tujuan" required autocomplete="off"><br/><br/></td>
            </tr>
            <tr> 
                <td>Jumlah</td>
                <td><input type="numeric" name="saldo" required onkeypress="return hanyaAngka(event)" autocomplete="off"><br/><br/></td>
            </tr>
            <tr> 
                <td>Keterangan</td>
                <td><textarea type="text" name="keterangan" rows="4" cols="23" autocomplete="off"></textarea><br/><br/></td>
            </tr>
            <tr> 
                <td></td>
                <td><input type="submit" name="Submit" value="Transfer"></td>
            </tr>
        </table>
    </form>
</body>
</html>
<script> 
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
</script> 