-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27 Des 2020 pada 15.11
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `errorhandler`
--

CREATE TABLE IF NOT EXISTS `errorhandler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` int(4) NOT NULL,
  `message` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `errorhandler`
--

INSERT INTO `errorhandler` (`id`, `kode`, `message`) VALUES
(1, 1062, 'Duplicate Entry Exception'),
(2, 1064, 'ID tujuan tidak valid');

-- --------------------------------------------------------

--
-- Struktur dari tabel `logactivities`
--

CREATE TABLE IF NOT EXISTS `logactivities` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) DEFAULT NULL,
  `activity` varchar(250) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data untuk tabel `logactivities`
--

INSERT INTO `logactivities` (`id`, `id_user`, `activity`, `date`) VALUES
(14, 5610137, 'User melakukan login pada 2020-12-27 13:43:57', '2020-12-27 13:43:57'),
(15, 3230224, 'User melakukan login pada 2020-12-27 13:47:23', '2020-12-27 13:47:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `saldo`
--

CREATE TABLE IF NOT EXISTS `saldo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `saldo` double NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `saldo`
--

INSERT INTO `saldo` (`id`, `id_user`, `saldo`, `updated_at`) VALUES
(3, 3230224, 998500, '2020-12-27 13:37:30'),
(4, 5610137, 100000, '2020-12-27 13:38:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trandetail`
--

CREATE TABLE IF NOT EXISTS `trandetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `jenis_transaksi` varchar(50) NOT NULL,
  `nominal` double NOT NULL,
  `id_tujuan` int(11) NOT NULL,
  `keterangan` varchar(250) NOT NULL,
  `status` varchar(10) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data untuk tabel `trandetail`
--

INSERT INTO `trandetail` (`id`, `id_user`, `jenis_transaksi`, `nominal`, `id_tujuan`, `keterangan`, `status`, `updated_at`, `created_at`) VALUES
(1, 5610137, 'Top Up', 10000, 5610137, 'Top Up Saldo', 'Success', '2020-12-26 22:57:51', '2020-12-26 22:57:51'),
(2, 5610137, 'Penarikan', 1000, 5610137, 'Penarikan Saldo', 'Success', '2020-12-26 23:02:23', '2020-12-26 23:02:23'),
(12, 3230224, 'Debit', 1000, 5610137, 'InhouseTrf CSCS dari PAIJO ', 'Success', '2020-12-27 06:09:05', '2020-12-27 06:09:05'),
(13, 3230224, 'Transfer', 1000, 3230224, 'InhouseTrf CSCS ke ULIL HIDAYAH ', 'Success', '2020-12-27 06:09:05', '2020-12-27 06:09:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(7) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `email_verified_at`, `created_at`, `updated_at`) VALUES
(3230224, 'Paijo', 'paijo@gmail.com', '$2y$10$cjpgV1QsC4a1lg1Qx7n94eqqlHqwSvbW.DMfd2BcMoZFy8Z3te1U.', '2SUEmhdPbtxyj9IWd7z0XZnXDnseesvdTucYHFHM', '2020-12-27 06:37:30', '2020-12-27 06:37:30', '2020-12-27 06:37:30'),
(5610137, 'Ulil Hidayah', 'ulilhidayah1998@gmail.com', '$2y$10$jEbXotYH1gaST19Z/hO0TeF03L.1BGPoH7Gmv2Zb.ekLcfrnuM30m', 'rNjPJvoaE4i3DH1ng8cUQBdsFO9VQYBHkQpc9XLG8qs6R106JxsbeSAr5s9n', '2020-12-27 06:38:49', '2020-12-27 06:38:49', '2020-12-27 06:47:12');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
